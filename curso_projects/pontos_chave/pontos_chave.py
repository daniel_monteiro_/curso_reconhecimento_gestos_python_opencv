import cv2
import matplotlib.pyplot as plt
import numpy as np

arquivo_proto = "../pose/body/mpi/pose_deploy_linevec_faster_4_stages.prototxt" # protocolo
arquivo_pesos = "../pose/body/mpi/pose_iter_160000.caffemodel" # modelo de treinamento

numero_pontos = 15

pares_pontos = [
    [0, 1], [1, 2], [2, 3], [3, 4], [1, 5], [5, 6], [6, 7], [1, 14],
    [14, 8], [8, 9], [9, 10], [14, 11], [11, 12], [12, 13]
]

cor_ponto, cor_linha = [255, 180, 0], [7, 62, 248]

imagem = cv2.imread("../imagens/body/single/single_3.jpg")

imagem_copia = np.copy(imagem)

imagem_largura = imagem.shape[1]
imagem_altura = imagem.shape[0]

modelo = cv2.dnn.readNetFromCaffe(arquivo_proto, arquivo_pesos)

altura_entrada = 368
largura_entrada = int((altura_entrada / imagem_altura) * imagem_largura)


# Converter a imgagem do tipo openCV para o formato blob caffe:
blob_entrada = cv2.dnn.blobFromImage(imagem, 1.0 / 255, (largura_entrada, altura_entrada), (0, 0, 0), swapRB=False,
                                     crop=False)

# Buscar saída:
modelo.setInput(blob_entrada)  # Passar a entrada para que a rede neural faça as previsões(vai processar toda a imagem
                                # na rede neural.)
saida = modelo.forward()  # respostas ou saída

print(saida.shape)  # (1, 44, 46, 82) #1 imagem, 44: mapas de saída, 46: mapas de afinidade

altura = saida.shape[2]
largura = saida.shape[3]


# Plotar saídas na imagem:

pontos = []
limite = 0.1  # limite mínimo que a confiança deve alcançar (10% de confiança)

for i in range(numero_pontos):
    mapa_confianca = saida[0, i, :, :]
    _, confianca, _, ponto = cv2.minMaxLoc(mapa_confianca)  # maior ponto de confiabilidade no mapa de confiança. Detecção mais viável.
    print(confianca)  # vai printar a confiança de detecção(%) de cada ponto encontrado no mapa de confiança da imagem.
    print(ponto)  # localização do ponto na imagem. Como coordenada na imagem.

    x = (imagem_largura * ponto[0]) / largura
    y = (imagem_altura * ponto[1]) / altura

    if confianca > limite:
        cv2.circle(imagem_copia, (int(x), int(y)), 8, cor_ponto, thickness=-1, lineType=cv2.FILLED)  # colocar um
                                                                                        # circulo no ponto/coordenada da
                                                                                        # imagem
        cv2.putText(imagem_copia, "{}".format(i), (int(x), int(y)), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 3,
                    lineType=cv2.LINE_AA)
        pontos.append((int(x), int(y)))
    else:
        pontos.append(None)

print(len(pontos))


# Esqueleto: Melhor identificação do corpo.

# Máscara para o esqueleto:
tamanho = cv2.resize(imagem, (imagem_largura, imagem_altura))
mapa_suave = cv2.GaussianBlur(tamanho, (3, 3), 0, 0)

mascara_mapa = np.uint8(mapa_suave > limite)  # máscara para valores maiores que o limite


# desenhar o esqueleto(ligar os pontos):
for par_ponto in pares_pontos:
    parteA = par_ponto[0]
    parteB = par_ponto[1]

    if pontos[parteA] and pontos[parteB]:  # se os valores existem
        cv2.line(imagem, pontos[parteA], pontos[parteB], cor_linha, 3)  # ligar dois pontos na imagem original
        cv2.circle(imagem, pontos[parteA], 8, cor_ponto, thickness=-1, lineType=cv2.LINE_AA)

        cv2.line(mascara_mapa, pontos[parteA], pontos[parteB], cor_linha, 3)  # fundo preto
        cv2.circle(mascara_mapa, pontos[parteA], 8, cor_ponto, thickness=-1, lineType=cv2.LINE_AA)


plt.figure(figsize=[14, 10])
plt.imshow(cv2.cvtColor(imagem_copia, cv2.COLOR_BGR2RGB))
plt.axis("off")
plt.savefig("pontos_chave_corpo.png")


plt.figure(figsize=[14, 10])
plt.imshow(cv2.cvtColor(imagem, cv2.COLOR_BGR2RGB))
plt.axis("off")
plt.savefig("pontos_chave_corpo_com_esqueleto.png")


plt.figure(figsize=[14, 10])
plt.imshow(cv2.cvtColor(imagem, cv2.COLOR_BGR2RGB))
plt.axis("off")
plt.savefig("mascara_pontos_chave_corpo.png")