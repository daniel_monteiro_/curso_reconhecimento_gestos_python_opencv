import cv2
import matplotlib.pyplot as plt

arquivo_proto = "pose/body/mpi/pose_deploy_linevec_faster_4_stages.prototxt" # protocolo
arquivo_pesos = "pose/body/mpi/pose_iter_160000.caffemodel" # modelo de treinamento

# imagem = cv2.imread("imagens/body/multiple/multiple_1.jpeg")
imagem = cv2.imread("imagens/body/single/single_1.jpg")

print(imagem.shape)


# cv2_imshow(imagem)


# cv2.imwrite("foto_teste.png", imagem)


imagem_largura = imagem.shape[1]
imagem_altura = imagem.shape[0]

# print(imagem_largura)
# print(imagem_altura)


modelo = cv2.dnn.readNetFromCaffe(arquivo_proto, arquivo_pesos)  # para obter modelo de rede neural treinada no caffe

altura_entrada = 368
largura_entrada = int((altura_entrada / imagem_altura) * imagem_largura)  # para ficar na mesma proporção


# conversão da imagem cv2 para Blob Caffe:
blob_entrada = cv2.dnn.blobFromImage(image=imagem, scalefactor=1.0 / 255,
                                     size=(largura_entrada, altura_entrada),
                                     mean=(0, 0, 0),
                                     swapRB=False, crop=False)  # scaleFactor: normalização dos dados. 255 porque os
                                                                # pixels variam de 0 a 255;
#
# mean: canais rgb, tecninca para combater as alterações de entrada de iluminação das imagens

# print(blob_entrada)
modelo.setInput(blob_entrada)  # para o modelo
saida = modelo.forward()  # previsão, que passa pela estrutura, que retorna a matriz 4D
saida_shape = saida.shape  # [id_da_imagem, mapa_confianca_afinidade, ]



# MAPA de confiança:
# ponto = 4  # pulso direito de acordo com o modelo MPII
ponto = 0  # cabeça de acordo com o modelo MPII
mapa_confianca = saida[0, ponto, :, :]
mapa_confianca = cv2.resize(mapa_confianca, (imagem_largura, imagem_altura))

plt.figure(figsize=[14, 10])
plt.imshow(cv2.cvtColor(imagem, cv2.COLOR_BGR2RGB))
plt.imshow(mapa_confianca, alpha=0.4)  # para mostrar a imagem original junto com o mapa de calor. Quanto mais perto do
                                       # 1 mais a imagem original se esconde e quanto mais perto do 0 a imagem original
                                       # surge.
plt.axis("off")

plt.savefig("figura_mapa_calor_confianca.png")




# MAPA de afinidade: (pontos a partir do 16 de acordo com o MPII)
# ponto = 16 # liga cabeça ao pescoço (observar toques)
ponto = 30 # liga cabeça ao ombro (observar toques)
mapa_confianca = saida[0, ponto, :, :]
mapa_confianca = cv2.resize(mapa_confianca, (imagem_largura, imagem_altura))

plt.figure(figsize=[14, 10])
plt.imshow(cv2.cvtColor(imagem, cv2.COLOR_BGR2RGB))
plt.imshow(mapa_confianca, alpha=0.4)  # para mostrar a imagem original junto com o mapa de calor. Quanto mais perto do
                                       # 1 mais a imagem original se esconde e quanto mais perto do 0 a imagem original
                                       # surge.
plt.axis("off")

plt.savefig("figura_mapa_calor_afinidade.png")




